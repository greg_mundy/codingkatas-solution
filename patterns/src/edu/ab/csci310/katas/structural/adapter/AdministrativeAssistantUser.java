package edu.ab.csci310.katas.structural.adapter;

import java.util.Date;

public class AdministrativeAssistantUser extends User {
	private Secretary secretary = new Secretary();

	@Override
	public Long getId() {
		return secretary.getSecretaryId();
	}

	@Override
	public void setId(Long id) {
		secretary.setSecretaryId(id);
	}

	@Override
	public String getFirstName() {
		return secretary.getSecretaryFirstName();
	}

	@Override
	public void setFirstName(String firstName) {
		secretary.setSecretaryFirstName(firstName);
	}

	@Override
	public String getLastName() {
		return secretary.getSecretaryLastName();
	}

	@Override
	public void setLastName(String lastName) {
		secretary.setSecretaryLastName(lastName);
	}

	@Override
	public Date getDateOfBirth() {
		return secretary.getSecretaryDateOfBirth();
	}

	@Override
	public void setDateOfBirth(Date dateOfBirth) {
		secretary.setSecretaryDateOfBirth(dateOfBirth);
	}

	@Override
	public String toString() {
		return (secretary.getSecretaryId() + " "
				+ secretary.getSecretaryFirstName() + " "
				+ secretary.getSecretaryLastName() + " " + secretary
					.getSecretaryDateOfBirth());
	}

	/**
	 * 
	 * @return The user's gender.
	 */
	public Gender getGender() {
		if (secretary.getSecretarySex().toUpperCase().equals("FEMALE")
				|| secretary.getSecretarySex().toUpperCase().equals("F")) {
			return Gender.FEMALE;
		} else if (secretary.getSecretarySex().toUpperCase().equals("MALE")
				|| secretary.getSecretarySex().toUpperCase().equals("M")) {
			return Gender.MALE;
		} else {
			return Gender.UNDEFINED;
		}
	}

	/**
	 * 
	 * @param gender
	 *            The user's gender.
	 */
	public void setGender(Gender gender) {
		if (gender == Gender.FEMALE) {
			secretary.setSecretarySex("FEMALE");
		} else if (gender == Gender.MALE) {
			secretary.setSecretarySex("MALE");
		} else {
			secretary.setSecretarySex("UNDEFINED");
		}
	}

	@Override
	public String getPositionDescription() {
		return "Perform adminstrative duties in support of the department.";
	}
}
