package edu.ab.csci310.katas.structural.adapter;

public class FacultyUser extends User {
	@Override
	public String getPositionDescription() {
		return "Teach a variety of lower to upper level undergraduate courses "
				+ " in area(s) of expertise.";
	}
}
