package edu.ab.csci310.katas.structural.adapter;

import java.util.Date;

/**
 * The {@linkplain Secretary} class is inherited from a legacy product.
 * 
 * @author gregorymundy
 *
 */
public class Secretary {
	private Long secretaryId;
	private String secretaryFirstName;
	private String secretaryLastName;
	private Date secretaryDateOfBirth;
	private String secretaryPositionDescription;
	private String secretarySex;

	public Long getSecretaryId() {
		return secretaryId;
	}

	public void setSecretaryId(Long secretaryId) {
		this.secretaryId = secretaryId;
	}

	public String getSecretaryFirstName() {
		return secretaryFirstName;
	}

	public void setSecretaryFirstName(String secretaryFirstName) {
		this.secretaryFirstName = secretaryFirstName;
	}

	public String getSecretaryLastName() {
		return secretaryLastName;
	}

	public void setSecretaryLastName(String secretaryLastName) {
		this.secretaryLastName = secretaryLastName;
	}

	public Date getSecretaryDateOfBirth() {
		return secretaryDateOfBirth;
	}

	public void setSecretaryDateOfBirth(Date secretaryDateOfBirth) {
		this.secretaryDateOfBirth = secretaryDateOfBirth;
	}

	public String getSecretaryPositionDescription() {
		return secretaryPositionDescription;
	}

	public void setSecretaryPositionDescription(
			String secretaryPositionDescription) {
		this.secretaryPositionDescription = secretaryPositionDescription;
	}

	public String getSecretarySex() {
		return secretarySex.toUpperCase();
	}

	public void setSecretarySex(String secretarySex) {
		this.secretarySex = secretarySex;
	}
}
