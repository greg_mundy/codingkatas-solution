package edu.ab.csci310.katas.structural.facade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScheduleMaker {
	private Map<Course, Room> courseRoom;

	public ScheduleMaker() {
		courseRoom = new HashMap<Course, Room>();
	}

	private List<Course> createCourses() {
		List<Course> courses = new ArrayList<Course>();
		Course course1 = new Course();
		Course course2 = new Course();
		Course course3 = new Course();
		Course course4 = new Course();
		Course course5 = new Course();

		course1.setId(1L);
		course1.setTitle("CSCI-261");
		course1.setDescription("Computer Science 1");

		course2.setId(2L);
		course2.setTitle("CSCI-262");
		course2.setDescription("Computer Science 2");

		course3.setId(3L);
		course3.setTitle("CSCI-263");
		course3.setDescription("Computer Science 3");

		course4.setId(4L);
		course4.setTitle("CSCI-310");
		course4.setDescription("Design Patterns");

		course5.setId(5L);
		course5.setTitle("CSCI-343");
		course5.setDescription("Database Management Systems");

		courses.add(course1);
		courses.add(course2);
		courses.add(course3);
		courses.add(course4);
		courses.add(course5);

		return courses;
	}

	private List<Room> createRooms() {
		List<Room> rooms = new ArrayList<Room>();

		Room room1 = new Room();
		Room room2 = new Room();
		Room room3 = new Room();

		room1.setBuilding("Kemper-Redd");
		room1.setNumber(101L);

		room2.setBuilding("Kemper-Redd");
		room2.setNumber(105L);

		room3.setBuilding("Withers-Brandon");
		room3.setNumber(101L);

		rooms.add(room1);
		rooms.add(room2);
		rooms.add(room3);

		return rooms;
	}

	public HashMap<Course, Room> getSchedule() {
		List<Room> rooms = createRooms();
		List<Course> courses = createCourses();

		courseRoom.put(courses.get(0), rooms.get(0));
		courseRoom.put(courses.get(1), rooms.get(0));
		courseRoom.put(courses.get(2), rooms.get(1));
		courseRoom.put(courses.get(3), rooms.get(1));
		courseRoom.put(courses.get(4), rooms.get(2));

		return (HashMap<Course, Room>) courseRoom;
	}
}