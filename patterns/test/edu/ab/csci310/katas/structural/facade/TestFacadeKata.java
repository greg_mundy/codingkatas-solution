package edu.ab.csci310.katas.structural.facade;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.junit.Test;

public class TestFacadeKata {

	@Test
	public void testScheduleMakerFacade() {
		ScheduleMaker scheduleMaker = new ScheduleMaker();
		HashMap<Course, Room> schedule = scheduleMaker.getSchedule();

		Iterator<Entry<Course, Room>> iterator = schedule.entrySet().iterator();

		while (iterator.hasNext()) {
			Entry<Course, Room> entry = iterator.next();
			System.out.println(entry.getKey().getTitle() + " "
					+ entry.getKey().getDescription());
		}

		assertNotNull(schedule);

	}
}
